#!/bin/sh
set -euxo pipefail

# Install dependencies
echo "Installing dependencies..."
pip install -r requirements.txt
pip install -r requirements-dev.txt

# Build wheel to wheelhouse
mkdir -p ../wheelhouse
pip wheel . --wheel-dir ../wheelhouse --no-deps