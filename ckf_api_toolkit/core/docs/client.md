# `Client` Class

Example:
```
class TestActionType(ActionType):
    GET = "GET"


class TestClient(Client):
    def __init__(self):
        super().__init__()
        self.add_action_type(TestActionType.GET, requests.get)
````