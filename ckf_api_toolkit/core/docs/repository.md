# `Repository` Class

Example:
```
class TestUseCases(UseCases):
    READ_FROM_API = 'READ_FROM_API'


class TestRepository(Repository):
    api_path: str

    def __init__(self):
        super().__init__()
        self.add_use_case(TestUseCases.READ_FROM_API, self.read_from_api)
        self.api_path = "http://an.api.url/"

    def read_from_api(self) -> Instruction:
        def __parser(api_response):
            return json.loads(api_response) # e.g. if your client didn't already do this

        return Instruction(TestActionType.GET, self.api_path, __parser)
```
