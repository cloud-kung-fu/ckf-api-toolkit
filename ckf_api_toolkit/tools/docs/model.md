# `Model` Abstract Base Class
This class is used as an abstract base class to be implemented by classes that represent a logical model of data entities.

## Example PyCharm Live Template
This [PyCharm][PyCharm] Live Template can be used in the application, and additionally as sample boilerplate for a standard implementation of a `Model` class for use with other tools from this toolkit:

```
from dataclasses import dataclass
from uuid import uuid4

from ckf_api_toolkit.tools.model import Model, ModelAttributes, model_initializer, validate_model_required_fields, \
    get_model_dict


@dataclass(frozen=True)
class $model$FrontEndValidationAttributes(ModelAttributes):
    $id_name$_id: str = $fe_id$


@dataclass(frozen=True)
class $model$FrontEndAttributes($model$FrontEndValidationAttributes):
    pass


@dataclass(frozen=True)
class $model$DatabaseAttributes(ModelAttributes):
    $id_name$_id: str = $db_id$


class $model$(Model):
    $id_name$_id: str
    $END$

    @classmethod
    def init_from_db(cls, model_dict: dict):
        return model_initializer(cls, model_dict, $model$DatabaseAttributes(), suppress_validation=True)

    @classmethod
    @validate_model_required_fields('$model$', vars($model$FrontEndValidationAttributes()).values())
    def init_from_front_end(cls, model_dict: dict):
        return model_initializer(cls, model_dict, $model$FrontEndAttributes())

    @classmethod
    def creation_init_from_front_end(cls, model_dict: dict):
        model_dict[$model$FrontEndAttributes.$id_name$_id] = str(uuid4())
        return cls.init_from_front_end(model_dict)

    def get_dict_for_front_end(self, *, private: bool = False) -> dict:
        public_attrs = []
        full_dict = get_model_dict(self, $model$FrontEndAttributes())
        return full_dict if private else {k: v for k, v in full_dict.items() if k in public_attrs}

    def get_dict_for_db(self) -> dict:
        return {**get_model_dict(self, $model$DatabaseAttributes())}

``` 

[PyCharm]: https://www.jetbrains.com/pycharm/