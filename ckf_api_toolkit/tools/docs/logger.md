# Logger
The `Logger` is a singleton that provides consistent levelled logging throughout the application.

## Log Levels
Log levels are provided by the `LogLevel` Enum. The levels are in order of lowest priority to highest:
- `debug`
- `info`
- `warn`
- `error`

Initialize the `Logger` singleton with the desired level. Only messages of the initialized priority or higher will be logged.

## Logging
Use `Logger().log()` (the parenthesis after `Logger` is required to invoke the singleton) to log messages

### Parameters
The `log()` function requires two parameters: the `LogLevel` and an `object` provided as the message.

### Title
The optional `title` kwarg provides a title to the log message.

### Formatting
The optional kwarg `pretty_json` is used to tell the logger to apply formatting to objects that conform to JSON serialization. **NOTE*: the `FORMAT_LOGGING` environment variable must be present and explicitly `'True'` (as a `str`) for this be formatted explicitly by `json.dumps()`. This is not done by default as many platforms will automatically format JSON strings in their logs.
