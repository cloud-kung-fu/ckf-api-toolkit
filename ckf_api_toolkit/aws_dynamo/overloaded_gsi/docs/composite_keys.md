# Composite Keys
Composite keys are prefect for nested one-to-many hierarchies.

## Access Pattern
This toolkit is opinionated on the setup of modelled entities using composite keys.
The root PK is the ID of the root node. The SK is the entire composite key. The 1st GSI PK is the name of the model, and the 1st GSI SK is the composite key. This allows for querying of only one model at any level via the GSI (as opposed to querying the primary sort key which also returns the root node).