"""
Tests basic composite key uses
"""
from unittest import TestCase

from ckf_api_toolkit.aws_dynamo.overloaded_gsi.composite_key_utils import get_dict_with_model_keys, \
    get_dict_with_composite_key


class TestCompositeKeyUtils(TestCase):
    """Test cases for composite key utils"""
    def test_get_dict_with_model_keys(self):
        """Test that get_dict_with_model_keys inserts entries for each key"""
        composite_key_param = 'any string'
        composite_key_default_delimiter = 'first#second#third'
        first_key_name = 'first key'
        second_key_name = 'second key'
        third_key_name = 'third key'

        input_dict = {
            composite_key_param: composite_key_default_delimiter,
            'any other key': 'any other value',
        }

        self.assertDictEqual(
            get_dict_with_model_keys(
                input_dict,
                composite_key_param,
                [first_key_name, second_key_name, third_key_name],
            ),
            {
                first_key_name: 'first',
                second_key_name: 'second',
                third_key_name: 'third',
                'any other key': 'any other value',
            }
        )

        composite_key_custom_delimiter = 'first@second@third'
        first_key_name = 'first key'
        second_key_name = 'second key'
        third_key_name = 'third key'

        input_dict = {
            composite_key_param: composite_key_custom_delimiter,
            'any other key': 'any other value',
        }

        self.assertDictEqual(
            get_dict_with_model_keys(
                input_dict,
                composite_key_param,
                [first_key_name, second_key_name, third_key_name],
                '@'
            ),
            {
                first_key_name: 'first',
                second_key_name: 'second',
                third_key_name: 'third',
                'any other key': 'any other value',
            }
        )

    def test_get_dict_with_composite_key(self):
        """Test that get_dict_with_composite_key inserts an entry for the composite key"""
        composite_key_param = 'any string'
        composite_key_default_delimiter = 'first#second#third'
        first_key_name = 'first key'
        second_key_name = 'second key'
        third_key_name = 'third key'

        input_dict = {
            first_key_name: 'first',
            second_key_name: 'second',
            third_key_name: 'third',
            'any other key': 'any other value',
        }

        self.assertDictEqual(
            get_dict_with_composite_key(
                input_dict,
                composite_key_param,
                [first_key_name, second_key_name, third_key_name],
            ),
            {
                composite_key_param: composite_key_default_delimiter,
                'any other key': 'any other value',
            }
        )

        composite_key_param = 'any string'
        composite_key_custom_delimiter = 'first@second@third'
        first_key_name = 'first key'
        second_key_name = 'second key'
        third_key_name = 'third key'

        input_dict = {
            first_key_name: 'first',
            second_key_name: 'second',
            third_key_name: 'third',
            'any other key': 'any other value',
        }

        self.assertDictEqual(
            get_dict_with_composite_key(
                input_dict,
                composite_key_param,
                [first_key_name, second_key_name, third_key_name],
                '@'
            ),
            {
                composite_key_param: composite_key_custom_delimiter,
                'any other key': 'any other value',
            }
        )
