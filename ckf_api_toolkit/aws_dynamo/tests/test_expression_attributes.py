"""
Tests for the ExpressionAttributes class
"""
from unittest import TestCase
from unittest.mock import patch
from uuid import UUID

from ckf_api_toolkit.aws_dynamo import expression_attributes
from ckf_api_toolkit.aws_dynamo.constants import DynamoScalarStringDataType

ExpressionAttributes = expression_attributes.ExpressionAttributes


class TestExpressionAttributes(TestCase):
    """
    Test cases for ExpressionAttributes
    """
    def setUp(self) -> None:
        """Inits TestExpressionAttributes with empty expression_attributes"""
        self.expression_attributes = ExpressionAttributes()

    TEST_UUID1 = UUID(int=1, version=4)
    TEST_UUID2 = UUID(int=2, version=4)
    TEST_UUID3 = UUID(int=3, version=4)

    @patch.object(expression_attributes, 'uuid4', return_value=TEST_UUID1, autospec=True)
    def test_set_expression_attribute_name(self, _):
        """Test that the expression attribute name gets set and can be retrieved"""
        test_name = 'test@#$name'
        test_attr_name = f"#{self.TEST_UUID1.hex}"
        expression_attr_name = self.expression_attributes.set_expression_attribute_name(test_name)
        expression_attribute_names = self.expression_attributes.expression_attribute_names
        self.assertEqual(expression_attr_name, test_attr_name)
        self.assertEqual(expression_attribute_names[test_attr_name], test_name)

    @patch.object(expression_attributes, 'uuid4', return_value=TEST_UUID1, autospec=True)
    def test_set_expression_attribute_value(self, _):
        """Test that the expression attribute value gets set and can be retrieved"""
        test_value = {DynamoScalarStringDataType.STRING.value: "A"}
        test_attr_value = f":{self.TEST_UUID1.hex}"
        expression_attr_value = self.expression_attributes.set_expression_attribute_value(test_value)
        expression_attribute_values = self.expression_attributes.expression_attribute_values
        self.assertEqual(expression_attr_value, test_attr_value)
        self.assertEqual(expression_attribute_values[test_attr_value], test_value)

    @patch.object(expression_attributes, 'uuid4', side_effect=[TEST_UUID1, TEST_UUID2, TEST_UUID3],
                  autospec=True)
    def test_set_nested_expression_attribute_name(self, _):
        """Test that nested expression attribute names get set and can be retrieved"""
        test_name1 = "test@#$1"
        test_name2 = "test@#$2"
        test_name3 = "test@#$3"
        test_path = f"{test_name1}.{test_name2}.{test_name3}"
        test_attr_name1 = f"#{self.TEST_UUID1.hex}"
        test_attr_name2 = f"#{self.TEST_UUID2.hex}"
        test_attr_name3 = f"#{self.TEST_UUID3.hex}"
        test_attr_full_name = f"{test_attr_name1}.{test_attr_name2}.{test_attr_name3}"
        expression_attr_name = self.expression_attributes.set_expression_attribute_name(test_path)
        expression_attribute_names = self.expression_attributes.expression_attribute_names
        self.assertEqual(expression_attr_name, test_attr_full_name)
        self.assertEqual(expression_attribute_names[test_attr_name1], test_name1)
        self.assertEqual(expression_attribute_names[test_attr_name2], test_name2)
        self.assertEqual(expression_attribute_names[test_attr_name3], test_name3)

    @patch.object(expression_attributes, 'uuid4', side_effect=[TEST_UUID1, TEST_UUID2],
                  autospec=True)
    def test_set_nested_indexed_expression_attribute_name(self, _):
        """Test that nested expression attribute names with indexes get set and can be retrieved"""
        test_name1 = "test@#$1"
        test_name1_index = "[321]"
        test_name2 = "test@#$2"
        test_name2_index = "[123]"
        test_path = f"{test_name1}{test_name1_index}.{test_name2}{test_name2_index}"
        test_attr_name1 = f"#{self.TEST_UUID1.hex}"
        test_attr_name2 = f"#{self.TEST_UUID2.hex}"
        test_attr_full_name = f"{test_attr_name1}{test_name1_index}.{test_attr_name2}{test_name2_index}"
        expression_attr_name = self.expression_attributes.set_expression_attribute_name(test_path)
        expression_attribute_names = self.expression_attributes.expression_attribute_names
        self.assertEqual(expression_attr_name, test_attr_full_name)
        self.assertEqual(expression_attribute_names[test_attr_name1], test_name1)
        self.assertEqual(expression_attribute_names[test_attr_name2], test_name2)

    @patch.object(expression_attributes, 'uuid4', return_value=TEST_UUID1,
                  autospec=True)
    def test_set_indexed_expression_attribute_name(self, _):
        """Test that an indexed expression attribute name gets set and can be retrieved"""
        test_name1 = "test@#$1"
        test_name1_index = "[321]"
        test_path = f"{test_name1}{test_name1_index}"
        test_attr_name1 = f"#{self.TEST_UUID1.hex}"
        test_attr_full_name = f"{test_attr_name1}{test_name1_index}"
        expression_attr_name = self.expression_attributes.set_expression_attribute_name(test_path)
        expression_attribute_names = self.expression_attributes.expression_attribute_names
        self.assertEqual(expression_attr_name, test_attr_full_name)
        self.assertEqual(expression_attribute_names[test_attr_name1], test_name1)
