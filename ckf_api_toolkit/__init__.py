"""
Welcome to the Cloud Kung Fu API Toolkit documentation. Here you'll find all of the Python docstrings for easy
reference.

If this isn't what you're looking for, try heading back to https://docs.cloudkungfu.io.
"""