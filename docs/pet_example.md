# Create a 'Pet' Example

```
from requests import Timeout

from ckf_api_toolkit.core import Repository, Actor, UseCase
from ckf_api_toolkit.http import HttpInstruction, RequestsClient, RequestsPayload, HttpActionType


class CreateAPetUseCase(UseCase):
    pet_name: str
    name = "Create a Pet"

    def __init__(self, pet_name: str):
        self.pet_name = pet_name


http_client = RequestsClient(0, 3)  # No retries, 3 second timeout


class PetDataStoreTimeout(Exception):
    pass


class PetDataStoreRepository(Repository):
    pet_service_url: str = 'https://AN_API_URL'

    def __init__(self):
        super(PetDataStoreRepository, self).__init__()
        self.add_error_conversion(Timeout, PetDataStoreTimeout)
        self.add_use_case(CreateAPetUseCase, self.create_pet)

    def create_pet(self, use_case: CreateAPetUseCase) -> HttpInstruction:
        def __parser(http_response) -> dict:
            # For this example we'll assume the API returns the data in a way that can be used directly;
            #  this is where you would do additional parsing as required
            return http_response.json()

        payload = RequestsPayload(self.pet_service_url)
        payload.add_data_key('petName', use_case.pet_name)

        return HttpInstruction(HttpActionType.POST, payload, __parser)


pet_actor = Actor(http_client, PetDataStoreRepository())


def create_a_pet_handler(pet_name: str) -> dict:
    try:
        new_pet = pet_actor.run_use_case(CreateAPetUseCase(pet_name))

    except PetDataStoreTimeout:
        # This is just to demonstrate catching a custom exception
        print('Oh no! Time out!')
        return {}

    return new_pet

```
